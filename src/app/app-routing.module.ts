import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListConferenceComponent} from './conference/list-conference/list-conference.component';
import {AddConferenceComponent} from './conference/add-conference/add-conference.component';
import {ConferenceDetailComponent} from './conference/conference-detail/conference-detail.component';
import {AddParticipantComponent} from './conference/add-participant/add-participant.component';
import {AddFeeComponent} from './conference/add-fee/add-fee.component';
import {LoginComponent} from './conference/login/login.component';

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'conferences', component: ListConferenceComponent},
  {path: 'conferences-add', component: AddConferenceComponent},
  {path: 'conferences/:id', component: ConferenceDetailComponent},
  {path: 'signup', component: AddParticipantComponent},
  {path: 'login', component: LoginComponent},
  {path: 'fees-add', component: AddFeeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
