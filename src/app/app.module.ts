import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ListConferenceComponent} from './conference/list-conference/list-conference.component';
import {AddConferenceComponent} from './conference/add-conference/add-conference.component';
import {ConferenceService} from './conference/shared/service/conference.service';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ConferenceDetailComponent} from './conference/conference-detail/conference-detail.component';
import {SectionService} from './conference/shared/service/section.service';
import {AddParticipantComponent} from './conference/add-participant/add-participant.component';
import {ParticipantService} from './conference/shared/service/participant.service';
import {LoginService} from './conference/shared/service/login.service';
import {AddFeeComponent} from './conference/add-fee/add-fee.component';
import {LoginComponent} from './conference/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ListConferenceComponent,
    AddConferenceComponent,
    ConferenceDetailComponent,
    AddParticipantComponent,
    AddFeeComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ConferenceService, SectionService, ParticipantService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
