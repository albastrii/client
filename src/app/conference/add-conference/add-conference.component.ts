import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ConferenceService} from '../shared/service/conference.service';
import {Conference} from '../shared/model/conference.model';
import {Fee} from '../shared/model/fee.model';

@Component({
  selector: 'app-add-conference',
  templateUrl: './add-conference.component.html',
  styleUrls: ['./add-conference.component.css']
})
export class AddConferenceComponent implements OnInit {
  constructor(private conferenceService: ConferenceService,
              private location: Location) {
  }

  ngOnInit() {
  }

  save(name, topic, description, location, startDate, endDate, amount, abstractDeadline, paperDeadline) {
    if (name === ''
      || topic === ''
      || description === ''
      || location === ''
      || startDate === ''
      || endDate === ''
      || amount === '') {
      console.log('aici');
      alert('All fields are required to be filled!');
      this.goBack();
    } else {
      const conference: Conference = {
        id: -1,
        name,
        topic,
        description,
        location,
        startDate,
        endDate,
        abstractDeadline,
        paperDeadline,
        fees: [],
        sections: [],
        proposals: [],
        pcMembers: [],
        amount: Number(amount)
      };
      this.conferenceService.save(conference)
        .subscribe(res => {
            this.goBack();
          },
          err => {
            this.goBack();
          });
    }
  }

  goBack(): void {
    this.location.back();
  }
}
