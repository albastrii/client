import {Component, OnInit} from '@angular/core';
import {ParticipantService} from '../shared/service/participant.service';
import {ConferenceService} from '../shared/service/conference.service';
import {Conference} from '../shared/model/conference.model';
import {Fee} from '../shared/model/fee.model';
import {Login} from '../shared/model/login.model';
import {Author} from '../shared/model/author.model';
import {Participant} from '../shared/model/participant.model';

@Component({
  selector: 'app-add-fee',
  templateUrl: './add-fee.component.html',
  styleUrls: ['./add-fee.component.css']
})
export class AddFeeComponent implements OnInit {
  errorMessage: string;
  conferences: Conference[];
  selectedConference: Conference;

  constructor(private participantService: ParticipantService, private conferenceService: ConferenceService) {
  }

  ngOnInit() {
    this.getConferences();
  }

  getConferences() {
    this.conferenceService.getConferences().subscribe(m => this.conferences = m, error => this.errorMessage = error);
  }

  saveFee() {
    if (this.selectedConference === undefined) {
      alert('No conference was selected!');
    } else {
      console.log(this.selectedConference);
      const login: Login = {username: 'a', password: 'a'};
      const participant: Participant = {
        id: 3,
        firstName: 'a',
        lastName: 'a',
        age: 1,
        fees: [],
        login
      };
      const fee: Fee = {id: -1, conference: this.selectedConference, participant, date: '2019-01-01'};
      console.log(fee);
      this.participantService.saveFee(fee).subscribe(res => {
          console.log(res);
        },
        err => {
          // alert('No conference was selected!');
          console.log(err);
        });
    }
  }
}
