import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ParticipantService} from '../shared/service/participant.service';
import {LoginService} from '../shared/service/login.service';
import {Conference} from '../shared/model/conference.model';
import {Listener} from '../shared/model/listener.model';
import {Login} from '../shared/model/login.model';
import {Author} from '../shared/model/author.model';
import {listener} from '@angular/core/src/render3';
import {Proposal} from '../shared/model/proposal.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-participant',
  templateUrl: './add-participant.component.html',
  styleUrls: ['./add-participant.component.css']
})
export class AddParticipantComponent implements OnInit {

  constructor(private participantService: ParticipantService,
              private location: Location, private router: Router) {
  }

  ngOnInit() {
  }

  saveListener(username, password, firstName, lastName, age) {
    if (username === '' ||
      password === '' ||
      firstName === '' ||
      lastName === '' ||
      age === '') {
      alert('All fields must me filled!');
    } else {
      if (!(!isNaN(parseFloat(age)) && isFinite(age))) {
      alert('Age should be a numeric value');
      } else {
        const login: Login = {username, password};
        const listen: Listener = {id: -1, firstName, lastName, age: Number(age), fees: [], sections: [], login};
        this.participantService.saveListener(listen)
          .subscribe(res => {
              this.router.navigate(['/conferences']);
            },
            err => {
              this.router.navigate(['/conferences']);
            });
      }

    }
  }

  saveAuthor(username, password, firstName, lastName, age, position) {
    const login: Login = {username, password};
    const author: Author = {
      id: -1,
      firstName,
      lastName,
      age: Number(age),
      fees: [],
      position,
      proposal: null,
      login
    };
    this.participantService.saveAuthor(author)
      .subscribe(res => {
          this.router.navigate(['/conferences']);
        },
        err => {
          this.router.navigate(['/conferences']);
        });
  }

  goBack(): void {
    this.location.back();
  }
}
