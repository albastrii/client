import {Component, Input, OnInit} from '@angular/core';
import {Conference} from '../shared/model/conference.model';
import {ActivatedRoute} from '@angular/router';
import {ConferenceService} from '../shared/service/conference.service';
import {SectionChair} from '../shared/model/sectionChair.model';
import {Section} from '../shared/model/section.model';

@Component({
  selector: 'app-conference-section',
  templateUrl: './conference-detail.component.html',
  styleUrls: ['./conference-detail.component.css']
})
export class ConferenceDetailComponent implements OnInit {
  conference: Conference;
  private conferenceService: ConferenceService;

  constructor(conferenceService: ConferenceService,
              private route: ActivatedRoute) {
    this.conferenceService = conferenceService;
  }

  ngOnInit() {
    this.get();
  }

  get(): void {
    const conferenceId = Number(this.route.snapshot.paramMap.get('id'));
    this.conferenceService.getConference(conferenceId)
      .subscribe(c => this.conference = c);
  }

  saveSection(name: string, firstName: string, lastName: string, age: string) {
    const sectionChair: SectionChair = {id: -1, firstName, lastName, age: Number(age)};
    const section: Section = {id: -1, name, sectionChair};
    const conferenceId = Number(this.route.snapshot.paramMap.get('id'));
    this.conferenceService.saveSection(conferenceId, section).subscribe(r =>  console.log('success!'), e => console.log('error'));
    window.location.reload();
  }
}
