import {Component, OnInit} from '@angular/core';
import {Conference} from '../shared/model/conference.model';
import {ConferenceService} from '../shared/service/conference.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-conference',
  templateUrl: './list-conference.component.html',
  styleUrls: ['./list-conference.component.css']
})
export class ListConferenceComponent implements OnInit {
  errorMessage: string;
  conferences: Conference[];
  selectedConference: Conference;

  constructor(private conferenceService: ConferenceService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getConferences();
  }

  getConferences() {
    this.conferenceService.getConferences().subscribe(m => this.conferences = m, error => this.errorMessage = error);
  }

  onSelect(conference: Conference) {
    this.selectedConference = conference;
    this.gotoDetail();
  }

  private gotoDetail(): void {
    this.router.navigate(['/conferences', this.selectedConference.id]);
  }
}
