import {Component, OnInit} from '@angular/core';
import {LoginService} from '../shared/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginError = false;

  constructor(private loginService: LoginService) {
  }

  ngOnInit() {
  }

  tryLogin(username, password) {
    this.loginService.login(username, password).subscribe(
      login => {
        localStorage.setItem('user', JSON.stringify(login));
        console.log(login);
      }, err => {
        console.log('wrong login');
        this.loginError = true;
      }
    );
  }
}
