import {Participant} from './participant.model';
import {Proposal} from './proposal.model';

export class Author extends Participant {
  id: number;
  position: string;
  proposal: Proposal;
}
