import {Proposal} from './proposal.model';
import {Reviewer} from './reviewer.model';

export class Bidding {
  id: number;
  reviewer: Reviewer;
  proposal: Proposal;
  result: string;
}
