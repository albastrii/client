import {PCMember} from './pcMember.model';

export class Chair extends PCMember {
  id: number;
  position: string;
}
