import {Fee} from './fee.model';
import {Section} from './section.model';
import {Proposal} from './proposal.model';
import {PCMember} from './pcMember.model';

export class Conference {
  id: number;
  name: string;
  topic: string;
  description: string;
  location: string;
  startDate: Date;
  endDate: Date;
  fees: Fee[];
  sections: Section[];
  proposals: Proposal[];
  pcMembers: PCMember[];
  amount: number;
  abstractDeadline: Date;
  paperDeadline: Date;
}
