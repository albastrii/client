import {Paper} from './paper.model';
import {Reviewer} from './reviewer.model';

export class Evaluation {
  id: number;
  reviewer: Reviewer;
  paper: Paper;
  result: string;
}
