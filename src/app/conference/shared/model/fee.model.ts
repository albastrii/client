import {Conference} from './conference.model';
import {Participant} from './participant.model';

export class Fee {
  id: number;
  conference: Conference;
  participant: Participant;
  date: string;
}
