import {Section} from './section.model';
import {Participant} from './participant.model';

export class Listener extends Participant {
  id: number;
  sections: Section[];
}
