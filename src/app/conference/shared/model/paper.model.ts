import {Evaluation} from './evaluation.model';
import {Proposal} from './proposal.model';

export class Paper {
  id: number;
  proposal: Proposal;
  title: string;
  document: string;
  evaluations: Evaluation[];
}
