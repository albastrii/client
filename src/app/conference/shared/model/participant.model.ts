import {Conference} from './conference.model';
import {Fee} from './fee.model';
import {Login} from './login.model';

export class Participant {
  id: number;
  fees: Fee[];
  firstName: string;
  lastName: string;
  age: number;

  login: Login;
}
