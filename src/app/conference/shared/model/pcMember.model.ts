import {Conference} from './conference.model';
import {Login} from './login.model';

export class PCMember {
  id: number;
  conferences: Conference[];
  name: string;
  affiliation: string;
  email: string;
  website: string;
  login: Login;
}
