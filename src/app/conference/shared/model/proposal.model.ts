import {Author} from './author.model';
import {Paper} from './paper.model';
import {Bidding} from './bidding.model';

export class Proposal {
  id: number;
  author: Author;
  title: string;
  description: string;
  abstractProposal: string;
  paper: Paper;
  biddings: Bidding[];
}
