import {Bidding} from './bidding.model';
import {Evaluation} from './evaluation.model';
import {PCMember} from './pcMember.model';

export class Reviewer extends PCMember{
  id: number;
  domainOfInterest: string;
  biddings: Bidding[];
  evaluations: Evaluation[];
}
