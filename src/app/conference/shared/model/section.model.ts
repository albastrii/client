import {SectionChair} from './sectionChair.model';

export class Section {
  id: number;
  name: string;
  sectionChair: SectionChair;
}
