export class SectionChair {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
}
