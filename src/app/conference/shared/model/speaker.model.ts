import {Author} from './author.model';
import {Section} from './section.model';
import {Paper} from './paper.model';

export class Speaker extends Author {
  id: number;
  section: Section;
  paper: Paper;
  presentation: string;
}
