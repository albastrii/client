import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Conference} from '../model/conference.model';
import {Section} from '../model/section.model';

@Injectable()
export class ConferenceService {
  private conferencesUrl = 'http://localhost:8080/api/conferences';
  private sectionsUrl = 'http://localhost:8080/api/sections';

  constructor(private httpClient: HttpClient) {
  }

  getConferences(): Observable<Conference[]> {
    return this.httpClient.get<Array<Conference>>(this.conferencesUrl);
  }

  getConference(id: number): Observable<Conference> {
    return this.httpClient.get<Conference>(`${this.conferencesUrl}/${id}`);
  }

  save(conference): Observable<Conference> {
    return this.httpClient.post<Conference>(this.conferencesUrl, conference);
  }

  saveSection(conferenceID, section): Observable<Section> {
    console.log(`${this.sectionsUrl}/?conferenceId=${conferenceID}`);
    return this.httpClient.post<Section>(`${this.sectionsUrl}/?conferenceId=${conferenceID}`, section);
  }
}
