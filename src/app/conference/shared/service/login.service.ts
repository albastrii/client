import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Listener} from '../model/listener.model';
import {Author} from '../model/author.model';
import {Login} from '../model/login.model';

@Injectable()
export class LoginService {
  private loginUrl = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient) {
  }

  login(username: string, password: string): Observable<Login> {
    return this.httpClient.get<Login>(`${this.loginUrl}/login?username=${username}&password=${password}`);
  }
}
