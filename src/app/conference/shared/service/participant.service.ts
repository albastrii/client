import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Listener} from '../model/listener.model';
import {Author} from '../model/author.model';
import {Fee} from '../model/fee.model';

@Injectable()
export class ParticipantService {
  private participantsUrl = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient) {
  }

  saveListener(listener): Observable<Listener> {
    return this.httpClient.post<Listener>(`${this.participantsUrl}/listeners`, listener);
  }

  saveAuthor(author): Observable<Author> {
    return this.httpClient.post<Author>(`${this.participantsUrl}/authors`, author);
  }

  saveFee(fee): Observable<Fee> {
    return this.httpClient.post<Fee>(`${this.participantsUrl}/fees`, fee);
  }
}
