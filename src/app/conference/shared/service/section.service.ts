import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Conference} from '../model/conference.model';
import {Section} from '../model/section.model';

@Injectable()
export class SectionService {
  private sectionsUrl = 'http://localhost:8080/api/sections';

  constructor(private httpClient: HttpClient) {
  }

  getSectionsByConferenceId(id: number): Observable<Section[]> {
    return this.httpClient.get<Array<Section>>(`${this.sectionsUrl}/?conferenceId=${id}`);
  }

  getSection(id: number): Observable<Section> {
    return this.httpClient.get<Section>(`${this.sectionsUrl}/${id}`);
  }

  save(section): Observable<Section> {
    return this.httpClient.post<Section>(this.sectionsUrl, section);
  }
}
