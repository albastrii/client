import {Component, OnInit} from '@angular/core';
import {LoginService} from '../conference/shared/service/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginError = false;
  emtpyFields = false;

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  tryLogin(username, password) {
    this.loginError = false;
    this.emtpyFields = false;
    if (username === '' || password === '') {
      this.emtpyFields = true;
    } else {
      this.loginService.login(username, password).subscribe(
        login => {
          localStorage.setItem('user', JSON.stringify(login));
          console.log(login);
          this.router.navigate(['/conferences']);
        }, err => {
          console.log('wrong login');
          this.loginError = true;
        }
      );
    }
  }
}
